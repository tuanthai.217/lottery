export const MENUS = [
    { id: 0, name: 'Home', url: '' },
    { id: 1, name: 'My Ticket', url: '/my-ticket' },
    { id: 2, name: 'Results', url: '/results' },
    { id: 3, name: 'Contact', url: '/contact' }
]

export const MAX_NUMBER = 45;
export const MAX_ITEM_TICKET = 6;