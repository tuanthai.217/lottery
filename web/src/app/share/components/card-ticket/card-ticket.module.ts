import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardTicketComponent } from './card-ticket.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    CardTicketComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    CardTicketComponent
  ]
})
export class CardTicketModule { }
