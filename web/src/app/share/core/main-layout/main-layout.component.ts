import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
    const hWindow = window.innerHeight;
    const hHeader = document.getElementById('main_header')?.offsetHeight || 0;
    const hFooter = document.getElementById('main_footer')?.offsetHeight || 0;
    const hRouter = document.getElementById('main_router');

    if (hRouter) {
      hRouter.style.minHeight = hWindow - (hHeader + hFooter) + 'px';
    }
  }
}
