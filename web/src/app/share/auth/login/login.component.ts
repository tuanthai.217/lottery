import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formLogin: FormGroup | any

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.

    this.initForm();
  }

  initForm() {
    this.formLogin = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required,])
    })
  }

  getErrorMessage() {
    if (this.formLogin.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.formLogin.email.hasError('email') ? 'Not a valid email' : '';
  }

  onSubmit(){
    console.log(this.formLogin.value);
    
  }
}
