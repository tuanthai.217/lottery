import { Component, OnInit } from '@angular/core';
import { MAX_ITEM_TICKET, MAX_NUMBER } from 'src/app/share/constants/constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  maxNumber = MAX_NUMBER;
  maxItemTicket = MAX_ITEM_TICKET;
  isAddTicket = false;
  currentTicket = 0;
  listNumbers: any = [];
  listSelected: any = [];

  constructor() { }

  ngOnInit(): void {
    this.createListNumber();
  }

  createListNumber() {
    for (let index = 1; index <= this.maxNumber; index++) {
      this.listNumbers.push(index)
    }
  }
  chooseNumber(number: any, currentTicket: number) {
    const idNumber = document.getElementById(`num-${number}`);

    //choose max 6 item
    if (this.listSelected[currentTicket]?.value.length < this.maxItemTicket) {
      //active number on selected and remove not select
      if (idNumber) {
        idNumber.classList.toggle('active-number');
        if (idNumber.classList.contains('active-number')) {
          this.listSelected[currentTicket].value.push(number);
        } else {
          let index = this.listSelected.indexOf(number);
          this.listSelected[currentTicket].value.splice(index, 1)
        }
      }
    } else if (idNumber && idNumber.classList.contains('active-number')) {
      idNumber.classList.toggle('active-number');
      let index = this.listSelected.indexOf(number);
      this.listSelected[currentTicket].value.splice(index, 1)
    }
  }

  deleteTicket(index: any) {
    this.listSelected.splice(index, 1);
    this.currentTicket -= 1;
    //remove class active-number
    document.querySelectorAll('.active-number').forEach((o: any) => {
      o.classList.remove('active-number')
    })
  }

  autoSelectTicket() {
    this.addTicket();

    //Random number of ticket
    for (let index = 0; index < this.maxItemTicket; index++) {
      let numRandom = Math.floor(Math.random() * this.maxNumber);

      //active item number ticket
      const idNumber = document.getElementById(`num-${numRandom}`);
      idNumber?.classList.add('active-number');

      //add numbers to the ticket
      this.listSelected[this.currentTicket].value.push(numRandom);
    }
  }

  addTicket() {
    const idListNumber = document.getElementById('listNumber');

    if (this.isAddTicket && idListNumber) {
      this.currentTicket += 1;

      // refresh number picker for tickets
      const childEles = idListNumber.querySelectorAll('.active-number');
      childEles.forEach((o: any) => {
        o.classList.remove("active-number")
      })
    } else {
      this.isAddTicket = true;
    }

    let newTicket = {
      ticketNumber: this.currentTicket,
      value: []
    }
    this.listSelected.push(newTicket)
  }

}
