import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyTicketComponent } from './my-ticket.component';
import { RouterModule, Routes } from '@angular/router';
import { CardTicketModule } from 'src/app/share/components/card-ticket/card-ticket.module';

const routes: Routes = [
  {
    path: '',
    component: MyTicketComponent
  }
]

@NgModule({
  declarations: [
    MyTicketComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CardTicketModule
  ]
})
export class MyTicketModule { }
