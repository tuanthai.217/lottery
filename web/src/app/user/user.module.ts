import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';
import { MainLayoutModule } from '../share/core/main-layout/main-layout.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    UserRoutingModule,
    MainLayoutModule
  ]
})
export class UserModule { }
