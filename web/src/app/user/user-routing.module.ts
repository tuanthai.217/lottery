import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from '../share/core/main-layout/main-layout.component';

const routes: Routes = [
    {
        path: '',
        component: MainLayoutComponent,
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
    },
    {
        path: 'login',
        component: MainLayoutComponent,
        loadChildren: () => import('../share/auth/login/login.module').then(m => m.LoginModule)
    },
    {
        path: 'my-ticket',
        component: MainLayoutComponent,
        loadChildren: () => import('./my-ticket/my-ticket.module').then(m => m.MyTicketModule)
    },
    {
        path: 'results',
        component: MainLayoutComponent,
        loadChildren: () => import('./results/results.module').then(m => m.ResultsModule)
    },
    {
        path: 'contact',
        component: MainLayoutComponent,
        loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule)
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
})
export class UserRoutingModule { }